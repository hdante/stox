/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_FIN_PARSER_STDIO_HH
#define STOX_FIN_PARSER_STDIO_HH

#include <memory>
#include <stdio.h>

#include "fin_parser.hh"

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

namespace stox {

class BovespaTimeSeriesFile {
	public:
	BovespaTimeSeriesFile(FILE *in_);
	BovespaTimeSeriesFile(BovespaTimeSeriesFile &&) = default;
	BovespaTimeSeriesFile(const BovespaTimeSeriesFile &) = delete;

	struct Iterator {
		Iterator();
		Iterator(FILE *in_);
		Iterator(const Iterator &) = default;
		Iterator &operator=(const Iterator &)  = default;
		const BovespaFormat &operator*() const;
		const BovespaFormat *operator->() const;
		Iterator &operator++();
		Iterator operator++(int);
		bool operator==(const Iterator &other);

		private:
		FILE *in = nullptr;
		BovespaFormat data;
	};

	Iterator begin();
	Iterator end();

	private:
	FILE *in;
};

struct FILEDeleter {
	void operator()(FILE *f);
};

typedef std::unique_ptr<FILE, FILEDeleter> UniqueFILE;
std::unique_ptr<FILE, FILEDeleter> make_unique_file(FILE *f);

} /* namespace stox */

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_FIN_PARSER_STDIO_HH */
