/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_FIN_PARSER_HH
#define STOX_FIN_PARSER_HH

#include <istream>
#include <iterator>
#include <string_view>
#include "fin_parser.h"

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

namespace stox {

template <typename... args> using IStreamIterator = std::istream_iterator<args...>;
using StringView = std::string_view;
using IStream = std::istream;
using OStream = std::ostream;

template <typename Number>
Number parse_fixed(const char *text, size_t len);

enum RecordType {
	RECORD_HEADER = 0,
	RECORD_QUOTE = 1,
	RECORD_FOOTER = 99,
};

enum BDIType {
	BDI_OPTION_CALL = 78,
	BDI_OPTION_PUT = 82,
};

class BovespaFormat: public stox_bovespa_format {
	public:
	unsigned short parse_bdi_type() const;
	unsigned long long parse_open() const;
	unsigned long long parse_close() const;
	unsigned long long parse_high() const;
	unsigned long long parse_low() const;
	unsigned long long parse_volume() const;
	unsigned long long parse_amount() const;
	StringView parse_code() const;
	unsigned short parse_trades() const;
	StringView parse_option_asset() const;
	StringView parse_option_asset_with_month() const;
	time_t parse_date() const;
	time_t parse_option_exercise_date() const;
	unsigned long long parse_strike() const;
	RecordType parse_record_type() const;
	StringView parse_file_name() const;
	bool operator==(const BovespaFormat &other) const;
	bool operator<(const BovespaFormat &other) const;
	int compare(const BovespaFormat &other) const;
};

class BovespaTimeSeries {
	public:
	BovespaTimeSeries(IStream &in_);
	BovespaTimeSeries(BovespaTimeSeries &&) = default;

	typedef IStreamIterator<BovespaFormat> Iterator;
	Iterator begin();
	Iterator end();

	protected:
	BovespaTimeSeries(const BovespaTimeSeries &) = delete;

	private:
	IStream &in;
};

template <typename Iter>
class QuoteIterator {
	public:
	QuoteIterator();
	QuoteIterator(Iter &iter_);
	QuoteIterator(const QuoteIterator &) = default;
	const BovespaFormat &operator*() const;
	const BovespaFormat *operator->() const;
	QuoteIterator &operator++();
	QuoteIterator operator++(int);
	bool operator==(const QuoteIterator &other) const;
	bool operator!=(const QuoteIterator &other) const;
	private:
	Iter end;
	Iter iter;
};

template <typename Iter>
QuoteIterator<Iter>::QuoteIterator(): iter(end) {}

template <typename Iter>
QuoteIterator<Iter>::QuoteIterator(Iter &iter_): iter(iter_) {
	while (iter != end && iter->parse_record_type() != RECORD_QUOTE)
		++iter;
}

template <typename Iter>
const BovespaFormat &QuoteIterator<Iter>::operator*() const
{
	return *iter;
}

template <typename Iter>
const BovespaFormat *QuoteIterator<Iter>::operator->() const
{
	return &(*iter);
}

template <typename Iter>
QuoteIterator<Iter> &QuoteIterator<Iter>::operator++()
{
	if (iter == end) return *this;

	do {
		++iter;
	} while (iter != end && iter->parse_record_type() != RECORD_QUOTE);

	return *this;
}

template <typename Iter>
QuoteIterator<Iter> QuoteIterator<Iter>::operator++(int)
{
	Iter old_iter{iter};
	QuoteIterator<Iter> old_quote{old_iter};

	++(*this);

	return old_quote;
}

template <typename Iter>
bool QuoteIterator<Iter>::operator==(const QuoteIterator<Iter> &other) const
{
	return (iter == other.iter);
}

template <typename Iter>
bool QuoteIterator<Iter>::operator!=(const QuoteIterator<Iter> &other) const
{
	return !(*this == other);
}

IStream &operator>>(IStream &in, BovespaFormat &out);
OStream &operator<<(OStream &out, const BovespaFormat &in);

} /* namespace stox */

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_FIN_PARSER_HH */
