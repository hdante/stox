/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_UTILS_HH
#define STOX_UTILS_HH

#include <istream>
#include <ostream>
#include <memory>
#include <set>
#include <stddef.h>
#include <vector>

#include "utils.h"

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

namespace stox {

template <typename... args> using Set = std::set<args...>;
template <typename... args> using Vector = std::vector<args...>;
using IStream = std::istream;
using OStream = std::ostream;

struct Currency {
	unsigned long long cents;
};

OStream &operator<<(OStream &, const Currency &);

template<typename Container, typename Compare>
Vector<typename Container::value_type> sorted(const Container &c, Compare comp)
{
	Vector<typename Container::value_type> s(c.size());
	partial_sort_copy(c.begin(), c.end(), s.begin(), s.end(), comp);
	return s;
}

template<typename T>
Set<T> operator-(const Set<T> &a, const Set<T> &b)
{
	Set<T> r;
	set_difference(a.begin(), a.end(), b.begin(), b.end(),
			inserter(r, r.begin()));
	return r;
}

template<typename T>
struct CompareByPointer {
	constexpr bool operator()( const std::unique_ptr<T>& lhs,
			const std::unique_ptr<T>& rhs ) const {
		return (*lhs) < (*rhs);
	}
};

} /* namespace stox */

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_UTILS_HH */
