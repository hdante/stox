/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_FIN_PARSER_H
#define STOX_FIN_PARSER_H

#include <sys/types.h>

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

#if defined(__cplusplus)
extern "C" {
#endif

struct stox_bovespa_header {
	char file_name[13];
	char market_name[8];
	char creation_date[8];
	char padding[214];
};

struct stox_bovespa_quote {
	char date[8];
	char bdi_type[2];
	char code[12];
	char market[3];
	char name[12];
	char type[10];
	char term_date[3];
	char currency[4];
	char open[13];
	char high[13];
	char low[13];
	char mean[13];
	char close[13];
	char bid[13];
	char ask[13];
	char trades[5];
	char amount[18];
	char volume[18];
	char strike[13];
	char adjust_index[1];
	char exercise_date[8];
	char adjust_factor[7];
	char secondary_strike[13];
	char isin_code[12];
	char sequence[3];
};

struct stox_bovespa_footer {
	char file_name[13];
	char market_name[8];
	char creation_date[8];
	char record_count[11];
	char padding[203];
};

struct stox_bovespa_format {
	char record_type[2];

	union {
		struct stox_bovespa_header header;
		struct stox_bovespa_quote quote;
		struct stox_bovespa_footer footer;
	};

	char newline[2];
};

struct stox_pascal_string {
	const char *str;
	size_t len;
};

unsigned long long stox_parse_fixed_ull(const char *text, size_t len);
unsigned stox_parse_fixed_u(const char *text, size_t len);
unsigned short stox_parse_fixed_us(const char *text, size_t len);
void stox_parse_pascal_string(const char *str, size_t buffsize,
	struct stox_pascal_string *pstr);
time_t stox_parse_date(const char *text);

#if defined(__cplusplus)
} /* extern "C" */
#endif

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_FIN_PARSER_H */
