/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_FIN_PARSER_STDIO_H
#define STOX_FIN_PARSER_STDIO_H

#include <stdio.h>

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

#if defined(__cplusplus)
extern "C" {
#endif

void stox_print_currency_with_symbols(FILE *out, unsigned long long v);
void stox_print_price_range(FILE *out, unsigned long long open,
		unsigned long long high, unsigned long long low,
		unsigned long long close);
void stox_print_trading_counts(FILE *out, unsigned long long amount,
		unsigned long long volume, unsigned trades);

#if defined(__cplusplus)
} /* extern "C" */
#endif

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_FIN_PARSER_STDIO_H */

