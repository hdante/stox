/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_ZIP_FILE_HH
#define STOX_ZIP_FILE_HH

#include <stdio.h>

#include <zip.h>

#include "stox/fin_parser_stdio.hh"

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

namespace stox {

class ZipFile {
	public:
	ZipFile(UniqueFILE &file);
	ZipFile(const ZipFile &) = delete;
	ZipFile &operator=(const ZipFile &) = delete;
	ZipFile(ZipFile &&) = default;
	ZipFile &operator=(ZipFile &&) = default;
	~ZipFile();

	class Iterator {
		public:
		Iterator();
		~Iterator();
		Iterator(zip_t *zip_);
		Iterator(const Iterator &) = default;
		Iterator &operator=(const Iterator &)  = default;
		zip_file_t *operator*() const;
		zip_file_t * const *operator->() const;
		Iterator &operator++();
		Iterator operator++(int);
		bool operator==(const Iterator &other);
		bool operator!=(const Iterator &other);

		private:
		zip_t *zip;
		zip_file_t *curr = nullptr;
		zip_int64_t count;
		zip_int64_t idx;
	};

	Iterator begin();
	Iterator end();

	private:
	zip_t *zip;
};

class BovespaTimeSeriesZip {
	public:
	BovespaTimeSeriesZip(zip_file_t *xfile_);
	BovespaTimeSeriesZip(BovespaTimeSeriesZip &&) = default;
	BovespaTimeSeriesZip(const BovespaTimeSeriesZip &) = delete;

	class Iterator {
		public:
		Iterator();
		Iterator(zip_file_t *xfile_);
		Iterator(const Iterator &) = default;
		Iterator &operator=(const Iterator &)  = default;
		const BovespaFormat &operator*() const;
		const BovespaFormat *operator->() const;
		Iterator &operator++();
		Iterator operator++(int);
		bool operator==(const Iterator &other) const;
		bool operator!=(const Iterator &other) const;

		private:
		zip_file_t *xfile;
		BovespaFormat data;
	};

	Iterator begin();
	Iterator end();

	private:
	zip_file_t *xfile;
};

} /* namespace stox */

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_ZIP_FILE_HH */
