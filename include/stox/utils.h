/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STOX_UTILS_H
#define STOX_UTILS_H

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

#if defined(__cplusplus)
extern "C" {
#endif

void stox_configure_sane_stdio(void);

#if defined(__cplusplus)
} /* extern "C" */
#endif

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STOX_UTILS_H */
