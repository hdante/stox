/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdexcept>
#include <stdio.h>

#include <zip.h>

#include "stox/fin_parser.hh"
#include "stox/zip_file.hh"

using namespace std;

namespace stox {

ZipFile::ZipFile(UniqueFILE &file)
{
	zip_error_t zerr;

	zip_error_init(&zerr);

	auto f = file.release();

	auto zsrc = zip_source_filep_create(f, 0, 0, &zerr);
	if (zsrc == nullptr) {
		file = make_unique_file(f);
		throw runtime_error(zip_error_strerror(&zerr));
	}

	zip = zip_open_from_source(zsrc, ZIP_CHECKCONS | ZIP_RDONLY, &zerr);
	if (zip == nullptr) {
		zip_source_free(zsrc);
		throw runtime_error(zip_error_strerror(&zerr));
	}

	zip_error_fini(&zerr);
}

ZipFile::~ZipFile()
{
	if (zip_close(zip) != 0) zip_close(zip);
}

ZipFile::Iterator::Iterator() {}

ZipFile::Iterator::~Iterator()
{
	if (curr != nullptr)
		zip_fclose(curr);
}

ZipFile::Iterator::Iterator(zip_t *zip_): zip(zip_) {
	count = zip_get_num_entries(zip, 0);
	idx = 0;

	if (idx < count)
		curr = zip_fopen_index(zip, idx, 0);
}

zip_file_t *ZipFile::Iterator::operator*() const
{
	return curr;
}

zip_file_t * const *ZipFile::Iterator::operator->() const
{
	return &curr;
}

ZipFile::Iterator &ZipFile::Iterator::operator++()
{
	if (curr == nullptr) return (*this);

	zip_fclose(curr);

	for (idx++; idx < count; idx++) {
		curr = zip_fopen_index(zip, idx, 0);
		if (curr != nullptr) return (*this);
	}

	curr = nullptr;
	return (*this);
}

ZipFile::Iterator ZipFile::Iterator::operator++(int)
{
	Iterator copy{*this};
	++(*this);
	return copy;
}

bool ZipFile::Iterator::operator==(const Iterator &other)
{
	if (curr == nullptr && other.curr == nullptr) return true;
	if (curr != nullptr && other.curr != nullptr) {
		if (zip == other.zip && idx == other.idx) return true;
	}

	return false;
}

bool ZipFile::Iterator::operator!=(const Iterator &other)
{
	return !(*this == other);
}

ZipFile::Iterator ZipFile::begin()
{
	return Iterator(zip);
}

ZipFile::Iterator ZipFile::end()
{
	return Iterator();
}

BovespaTimeSeriesZip::BovespaTimeSeriesZip(zip_file_t *xfile_): xfile(xfile_) {}

BovespaTimeSeriesZip::Iterator::Iterator(): xfile(nullptr) {}

BovespaTimeSeriesZip::Iterator::Iterator(zip_file_t *xfile_): xfile(xfile_)
{
	if (xfile != nullptr)
		++(*this);
}

const BovespaFormat &BovespaTimeSeriesZip::Iterator::operator*() const
{
	return data;
}

const BovespaFormat *BovespaTimeSeriesZip::Iterator::operator->() const
{
	return &data;
}

BovespaTimeSeriesZip::Iterator &BovespaTimeSeriesZip::Iterator::operator++()
{
	if (xfile == nullptr) return *this;

	char *p = (char *)&data;
	zip_uint64_t s = sizeof(data);
	zip_uint64_t i;
	zip_int64_t r;

	for(i = 0; i < s; i += (zip_uint64_t)r) {
		r = zip_fread(xfile, p+i, s-i);
		if (r < 1) break;
	}

	if (i < s) xfile = nullptr;

	return *this;
}

BovespaTimeSeriesZip::Iterator BovespaTimeSeriesZip::Iterator::operator++(int)
{
	Iterator copy{*this};
	++(*this);

	return copy;
}

bool BovespaTimeSeriesZip::Iterator::operator==(const BovespaTimeSeriesZip::Iterator &other)
	const
{
	if (xfile == nullptr && other.xfile == nullptr) return true;
	if (xfile != other.xfile) return false;

	return (data == other.data);
}

bool BovespaTimeSeriesZip::Iterator::operator!=(const BovespaTimeSeriesZip::Iterator &other)
	const
{
	return !(*this == other);
}

BovespaTimeSeriesZip::Iterator BovespaTimeSeriesZip::begin()
{
	return Iterator(xfile);
}

BovespaTimeSeriesZip::Iterator BovespaTimeSeriesZip::end()
{
	return Iterator();
}

} /* namespace stox */
