/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iomanip>
#include <iostream>
#include <memory>

#ifdef __GLIBCXX__
#include <ext/stdio_filebuf.h>
using stdio_filebuf = __gnu_cxx::stdio_filebuf<char>;
#endif

#ifdef _WIN32
#include <fcntl.h>
#endif

#include <stox/utils.hh>

using namespace std;

#define STOX_BUFFER_SIZE (1L<<16)

namespace stox {

OStream &operator<<(OStream &os, const Currency &cur)
{
	vector<string> suffixes = {"", "k", "M", "B", "T", "Q"};
	unsigned i;
	unsigned long long v = cur.cents;

	for (i = 0; i < size(suffixes)-1; i++) {
		if (v < 1000000) {
			auto old = os.fill();
			os << (v/100) << '.' << setw(2) << setfill('0') << (v%100)
				<< setfill(old) << suffixes[i];
			return os;
		}
		v /= 1000;
	}

	auto old = os.fill();
	os << (v/100) << '.' << setw(2) << setfill('0') << (v%100)
		<< setfill(old) << suffixes[i];
	return os;
}

} /* namespace stox */

void stox_configure_sane_stdio(void)
{
	#ifdef _WIN32
	_setmode(_fileno(stdin), _O_BINARY);
	_setmode(_fileno(stdout), _O_BINARY);
	#endif

	#ifdef __GLIBCXX__
	/* Required by GNU libstdc++ because pubsetbuf() is ignored */
	auto oldsbuf = cin.rdbuf();
	auto stdbuf = dynamic_cast<stdio_filebuf *>(oldsbuf);

	if (stdbuf != nullptr && stdbuf->is_open()) {
		static auto newsb = stdio_filebuf{stdbuf->fd(),
			ios_base::in | ios_base::binary, STOX_BUFFER_SIZE};
		cin.rdbuf(&newsb);
		return;
	}
	#endif

	static char stdio_buffer[STOX_BUFFER_SIZE];
	cin.rdbuf()->pubsetbuf(stdio_buffer, STOX_BUFFER_SIZE);
}
