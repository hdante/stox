/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <charconv>
#include <memory>
#include <string.h>
#include <time.h>

#include "stox/fin_parser.h"
#include "stox/fin_parser.hh"
#include "stox/fin_parser_stdio.h"
#include "stox/fin_parser_stdio.hh"

#define countof(x) (sizeof(x)/sizeof(x[0]))

using namespace std;

namespace stox {

static StringView parse_string_view(const char *str, size_t buffsize)
{
	StringView sv{str, buffsize};
	sv = sv.substr(0, sv.find(' '));
	return sv;
}


template <typename Number>
Number parse_fixed(const char *text, size_t len)
{
	Number rc = Number(-1);
	from_chars(text, text+len, rc);
	return rc;
}

template int parse_fixed(const char *text, size_t len);
template unsigned parse_fixed(const char *text, size_t len);
template unsigned short parse_fixed(const char *text, size_t len);

/* FIXME: work around clang++ issue that emits library multiply call and slows
 * down execution. Remove this when/if fixed. */
template<>
unsigned long long parse_fixed<unsigned long long>(const char *text, size_t len)
{
	unsigned long long v;
	unsigned i;

	v = 0;

	for (i = 0; i < len; i++) {
		v *= 10;
		v += text[i]-'0';
	}

	return v;
}

/* Work around mktime() slowness with DateCache class */
static class DateCache {
	DateCache(const DateCache &) = delete;
	static const size_t SIZE = 4;

	struct {
		char text[8];
		time_t time;
	} item[SIZE];

	size_t pos;

	public:
	DateCache() = default;
	time_t parse_date(const char *text) {
		for (size_t i = 0; i < SIZE; i++) {
			if (memcmp(text, item[i].text, sizeof(item[0].text)) == 0) {
				return item[i].time;
			}
		}
		time_t year = parse_fixed<unsigned>(text, 4)-1900;
		time_t month = parse_fixed<unsigned>(text+4, 2)-1;
		time_t day = parse_fixed<unsigned>(text+6, 2);
		struct tm tm;
		tm.tm_sec = tm.tm_min = tm.tm_hour = tm.tm_isdst = 0;
		tm.tm_year = year;
		tm.tm_mon = month;
		tm.tm_mday = day;

		memmove(item[pos].text, text, sizeof(item[0].text));
		auto &v = item[pos].time = mktime(&tm);
		pos = (pos+1)%SIZE;
		return v;
	}
} date_cache;

RecordType BovespaFormat::parse_record_type() const
{
	auto v = stox_parse_fixed_us(record_type, sizeof(record_type));

	switch(v) {
	case RECORD_HEADER:
	case RECORD_QUOTE:
	case RECORD_FOOTER:
		return RecordType(v);
	default:
		break;
	}

	throw invalid_argument("invalid record type");
}

unsigned long long BovespaFormat::parse_open() const
{
	return stox_parse_fixed_ull(quote.open, sizeof(quote.open));
}

unsigned long long BovespaFormat::parse_close() const
{
	return stox_parse_fixed_ull(quote.close, sizeof(quote.close));
}

unsigned long long BovespaFormat::parse_high() const
{
	return stox_parse_fixed_ull(quote.high, sizeof(quote.high));
}

unsigned long long BovespaFormat::parse_low() const
{
	return stox_parse_fixed_ull(quote.low, sizeof(quote.low));
}

unsigned short BovespaFormat::parse_bdi_type() const
{
	return parse_fixed<unsigned short>(quote.bdi_type, sizeof(quote.bdi_type));
}

unsigned long long BovespaFormat::parse_volume() const
{
	return stox_parse_fixed_ull(quote.volume, sizeof(quote.volume));
}

unsigned long long BovespaFormat::parse_amount() const
{
	return stox_parse_fixed_ull(quote.amount, sizeof(quote.amount));
}

unsigned long long BovespaFormat::parse_strike() const
{
	return stox_parse_fixed_ull(quote.strike, sizeof(quote.strike));
}

StringView BovespaFormat::parse_code() const
{
	return parse_string_view(quote.code, sizeof(quote.code));
}

unsigned short BovespaFormat::parse_trades() const
{
	return parse_fixed<unsigned short>(quote.trades, sizeof(quote.trades));
}

StringView BovespaFormat::parse_option_asset() const
{
	return StringView(quote.code, 4);
}

StringView BovespaFormat::parse_option_asset_with_month() const
{
	return StringView(quote.code, 5);
}

time_t BovespaFormat::parse_date() const
{
	return stox_parse_date(quote.date);
}

time_t BovespaFormat::parse_option_exercise_date() const
{
	return stox_parse_date(quote.exercise_date);
}

StringView BovespaFormat::parse_file_name() const
{
	return StringView(header.file_name, sizeof(header.file_name));
}

bool BovespaFormat::operator==(const BovespaFormat &other) const
{
	return compare(other) == 0;
}

bool BovespaFormat::operator<(const BovespaFormat &other) const
{
	return compare(other) < 0;
}

int BovespaFormat::compare(const BovespaFormat &other) const
{
	int r;
	auto t1 = parse_record_type();
	auto t2 = other.parse_record_type();
	if (t1 != t2) return t1-t2;

	if (t1 == RECORD_HEADER || t1 == RECORD_FOOTER) {
		auto &a = this->footer;
		auto &b = other.footer;
		r = memcmp(&a.market_name, &b.market_name, sizeof(a.market_name));
		if (r != 0) return r;
		r = memcmp(&a.file_name, &b.file_name, sizeof(a.file_name));
		if (r != 0) return r;
		r = memcmp(&a.creation_date, &b.creation_date, sizeof(a.creation_date));
		if (r != 0) return r;
		r = memcmp(&a.record_count, &b.record_count, sizeof(a.record_count));
		return r;
	}

	auto &a = this->quote;
	auto &b = other.quote;

	r = memcmp(&a.date, &b.date, sizeof(a.date));
	if (r != 0) return r;
	r = memcmp(&a.code, &b.code, sizeof(a.code));
	if (r != 0) return r;
	r = memcmp(&a.name, &b.name, sizeof(a.name));
	if (r != 0) return r;
	r = memcmp(&a.type, &b.type, sizeof(a.type));
	if (r != 0) return r;
	r = memcmp(&a.market, &b.market, sizeof(a.market));
	if (r != 0) return r;
	r = memcmp(&a.bdi_type, &b.bdi_type, sizeof(a.bdi_type));
	if (r != 0) return r;
	r = memcmp(&a.term_date, &b.term_date, sizeof(a.term_date));
	if (r != 0) return r;
	r = memcmp(&a.exercise_date, &b.exercise_date, sizeof(a.exercise_date));
	if (r != 0) return r;
	r = memcmp(&a.currency, &b.currency, sizeof(a.currency));
	if (r != 0) return r;
	r = memcmp(&a.strike, &b.strike, sizeof(a.strike));
	if (r != 0) return r;
	r = memcmp(&a.secondary_strike, &b.secondary_strike,
			sizeof(a.secondary_strike));
	if (r != 0) return r;
	r = memcmp(&a.adjust_index, &b.adjust_index, sizeof(a.adjust_index));
	if (r != 0) return r;
	r = memcmp(&a.adjust_factor, &b.adjust_factor, sizeof(a.adjust_factor));
	if (r != 0) return r;
	r = memcmp(&a.open, &b.open, sizeof(a.open));
	if (r != 0) return r;
	r = memcmp(&a.high, &b.high, sizeof(a.high));
	if (r != 0) return r;
	r = memcmp(&a.low, &b.low, sizeof(a.low));
	if (r != 0) return r;
	r = memcmp(&a.mean, &b.mean, sizeof(a.mean));
	if (r != 0) return r;
	r = memcmp(&a.close, &b.close, sizeof(a.close));
	if (r != 0) return r;
	r = memcmp(&a.bid, &b.bid, sizeof(a.bid));
	if (r != 0) return r;
	r = memcmp(&a.ask, &b.ask, sizeof(a.ask));
	if (r != 0) return r;
	r = memcmp(&a.trades, &b.trades, sizeof(a.trades));
	if (r != 0) return r;
	r = memcmp(&a.amount, &b.amount, sizeof(a.amount));
	if (r != 0) return r;
	r = memcmp(&a.volume, &b.volume, sizeof(a.volume));
	if (r != 0) return r;
	r = memcmp(&a.isin_code, &b.isin_code, sizeof(a.isin_code));
	if (r != 0) return r;
	r = memcmp(&a.sequence, &b.sequence, sizeof(a.sequence));

	return r;
}

BovespaTimeSeries::BovespaTimeSeries(IStream &in_): in(in_) {}

BovespaTimeSeries::Iterator BovespaTimeSeries::begin()
{
	return Iterator(in);
}

BovespaTimeSeries::Iterator BovespaTimeSeries::end()
{
	return Iterator();
}

IStream &operator>>(IStream &in, BovespaFormat &out)
{
	in.read((char*)&out, sizeof(BovespaFormat));
	return in;
}

OStream &operator<<(OStream &out, const BovespaFormat &in)
{
	out.write((char*)&in, sizeof(BovespaFormat));
	return out;
}

BovespaTimeSeriesFile::Iterator BovespaTimeSeriesFile::begin()
{
	return Iterator(in);
}

BovespaTimeSeriesFile::Iterator BovespaTimeSeriesFile::end()
{
	return Iterator();
}

BovespaTimeSeriesFile::Iterator::Iterator() {}

BovespaTimeSeriesFile::Iterator::Iterator(FILE *in_) : in(in_)
{
	if (in != nullptr)
		++(*this);
}


const BovespaFormat &BovespaTimeSeriesFile::Iterator::operator*() const
{
	return data;
}

const BovespaFormat *BovespaTimeSeriesFile::Iterator::operator->() const
{
	return &data;
}

BovespaTimeSeriesFile::Iterator &BovespaTimeSeriesFile::Iterator::operator++()
{
	if (in != nullptr && fread(&data, sizeof(data), 1, in) < 1)
		in = nullptr;

	return *this;
}

BovespaTimeSeriesFile::Iterator BovespaTimeSeriesFile::Iterator::operator++(int)
{
	Iterator copy{*this};
	++(*this);

	return copy;
}

bool BovespaTimeSeriesFile::Iterator::operator==(const BovespaTimeSeriesFile::Iterator &other)
{
	if (in == nullptr && other.in == nullptr) return true;
	if (in != other.in) return false;

	return (data == other.data);
}

void FILEDeleter::operator()(FILE *f)
{
	if (f == nullptr) return;

	if (fclose(f) != 0)
		fclose(f);
}

unique_ptr<FILE, FILEDeleter> make_unique_file(FILE *f)
{
	return unique_ptr<FILE, FILEDeleter>(f);
}

} /* namespace stox */

using namespace stox;

unsigned long long stox_parse_fixed_ull(const char *text, size_t len)
{
	return parse_fixed<unsigned long long>(text, len);
}

unsigned stox_parse_fixed_u(const char *text, size_t len)
{
	return parse_fixed<unsigned>(text, len);
}

unsigned short stox_parse_fixed_us(const char *text, size_t len)
{
	return parse_fixed<unsigned short>(text, len);
}

void stox_parse_pascal_string(const char *str, size_t buffsize,
	struct stox_pascal_string *pstr)
{
	auto sv = parse_string_view(str, buffsize);
	*pstr = stox_pascal_string{data(sv), size(sv)};
}

void stox_print_currency_with_symbols(FILE *out, unsigned long long v)
{
	const char *suffixes[] = {"", "k", "M", "B", "T", "Q"};
	unsigned i;

	for (i = 0; i < countof(suffixes)-1; i++) {
		if (v < 1000000) {
			fprintf(out, "%llu.%02llu%s", v/100, v%100, suffixes[i]);
			return;
		}
		v /= 1000;
	}

	fprintf(out, "%llu.%02llu%s", v/100, v%100, suffixes[i]);
}

void stox_print_price_range(FILE *out, unsigned long long open,
		unsigned long long high, unsigned long long low,
		unsigned long long close)
{
	fputc('[', out);
	stox_print_currency_with_symbols(out, open);
	fputc(' ', out);
	stox_print_currency_with_symbols(out, high);
	fputc(' ', out);
	stox_print_currency_with_symbols(out, low);
	fputc(' ', out);
	stox_print_currency_with_symbols(out, close);
	fputc(']', out);
}

void stox_print_trading_counts(FILE *out, unsigned long long amount,
		unsigned long long volume, unsigned trades)
{
	fprintf(out, "%llu ", amount);
	stox_print_currency_with_symbols(out, volume);
	fprintf(out, " (%u)", trades);
}

time_t stox_parse_date(const char *text)
{
	return date_cache.parse_date(text);
}
