/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <istream>
#include <iostream>

#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QCandlestickSeries>
#include <QtCharts/QCandlestickSet>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCore/QDateTime>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>

#include <stox/fin_parser.hh>
#include <stox/utils.hh>

using namespace std;
using namespace stox;
using namespace QtCharts;

QDate to_qdate(const BovespaFormat &item) {
	auto strdate = QString::fromUtf8(item.quote.date, sizeof(item.quote.date));
	return QDate::fromString(strdate, "yyyyMMdd");
}

QCandlestickSet *to_candlestick(const BovespaFormat &item) {
	return new QCandlestickSet{(qreal)item.parse_open()/100.0,
		(qreal)item.parse_high()/100.0, (qreal)item.parse_low()/100.0,\
		(qreal)item.parse_close()/100.0, (qreal)item.parse_date()};
}

int main(int argc, char *argv[])
{
	ios::sync_with_stdio(false);
	stox_configure_sane_stdio();

	istream *in;
	ifstream file;
	string_view code;

	QApplication qapp(argc, argv);

	if (argc > 1) {
		code = string_view(argv[1]);
	}
	else {
		code = "PETR4"sv;
	}

	if (argc < 3)
		in = &cin;
	else {
		in = &file;
		file.open(argv[2], ios_base::in | ios_base::binary);
	}

	auto *series = new QCandlestickSeries();
	series->setName(QString::fromLocal8Bit(code.data(), code.size()));
	series->setIncreasingColor(QColor(Qt::green));
	series->setDecreasingColor(QColor(Qt::red));

	QStringList dates;

	for (auto item: BovespaTimeSeries(*in)) {
		if (item.parse_code() != code) continue;
		series->append(to_candlestick(item));
		auto datetext = to_qdate(item).toString("dd/MM");
		dates << datetext;
	}

	QChart *chart = new QChart();
	chart->addSeries(series);
	chart->createDefaultAxes();

	auto *axis = qobject_cast<QBarCategoryAxis *>(chart->axes(Qt::Horizontal).at(0));
	axis->setCategories(dates);
	auto m = max(0, axis->count()-20);
	axis->setMin(axis->at(m));

	QValueAxis *axisY = qobject_cast<QValueAxis *>(chart->axes(Qt::Vertical).at(0));
	axisY->setMax(axisY->max() * 1.01);
	axisY->setMin(axisY->min() * 0.99);
	//! [4]

	//! [5]
	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);
	//! [5]

	//! [6]
	QChartView *chart_view = new QChartView(chart);
	chart_view->setRenderHint(QPainter::Antialiasing);

	QMainWindow window;
	window.setCentralWidget(chart_view);
	window.resize(800, 600);
	window.show();
	return qapp.exec();
}
