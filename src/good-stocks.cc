/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <unordered_map>

#include <stox/fin_parser.hh>
#include <stox/utils.hh>

using namespace std;
using namespace stox;

template<typename K, typename V>
static void insert_min(unordered_map<K, V> &map, const K &key, const V &value)
{
	auto res = map.emplace(key, value);
	if (!res.second) {
		res.first->second = min(res.first->second, value);
	}
}

static string fmt_volume(unsigned long long volume)
{
	auto v = (volume+50000)/100000;
	return to_string(v) + "K";
}

int main(int argc, char *argv[])
{
	set<string> stocks;
	set<string> bad;
	unordered_map<string, unsigned long long> volumes;

	bool invert;

	stox_configure_sane_stdio();

	if (argc > 1 && string(argv[1]) == "-b")
		invert = true;
	else
		invert = false;

	for (auto &item: BovespaTimeSeries(cin)) {
		try {
			auto type = item.parse_bdi_type();
			if (type != 2) continue;
			if (strncmp(item.quote.currency, "R$", 2) != 0) continue;
		}
		catch (invalid_argument&) {
			continue;
		}

		auto volume = item.parse_volume();
		auto code = string(item.parse_code());

		if (volume < 200000000ull)
			bad.insert(code);

		stocks.insert(code);
		insert_min(volumes, code, volume);
	}

	set<string> good;
	if (!invert) {
		good = stocks-bad;
	}
	else {
		good = bad;
	}
	auto vgood = sorted(good, [&](const string &a, const string &b) {
			return volumes[a]>volumes[b];
	});
	for (auto &s : vgood) {
		cout << s << ": " << fmt_volume(volumes[s]) << '\n';
	}

	return 0;
}
