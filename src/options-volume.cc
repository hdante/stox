/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <memory>
#include <unordered_map>
#include <unordered_set>

#include <stox/fin_parser.hh>
#include <stox/utils.hh>

#define BUFF_SIZE (1<<18)

template <typename... args> using UnorderedSet = std::unordered_set<args...>;
template <typename... args> using UnorderedMap = std::unordered_map<args...>;

using String = std::string;
using SystemClock = std::chrono::system_clock;
using namespace std;
using namespace stox;

struct OptionData {
	unsigned long long volume;
	unsigned long long amount;
	unsigned long long strike;
	unsigned trades;
	unsigned days;
};

OptionData &operator+=(OptionData &a, const OptionData &b)
{
	a.strike = b.strike;
	a.volume += b.volume;
	a.amount += b.amount;
	a.trades += b.trades;
	a.days += b.days;

	return a;
}

int run(istream &in)
{
	UnorderedSet<String> stocks(10000);
	UnorderedMap<String, OptionData> data(10000);

	auto now = SystemClock::to_time_t(SystemClock::now());

	for (auto &item: BovespaTimeSeries(in)) {
		try {
			auto type = item.parse_bdi_type();
			if (type != BDI_OPTION_CALL && type != BDI_OPTION_PUT) continue;
			auto date = item.parse_date();
			if (now-date > 2000000) continue;
			auto exercise_date = item.parse_option_exercise_date();
			if (exercise_date < now) continue;
		}
		catch (invalid_argument&) {
			continue;
		}

		auto volume = item.parse_volume();
		auto amount = item.parse_amount();
		auto trades = item.parse_trades();
		auto strike = item.parse_strike();
		auto code = string(item.parse_code());

		stocks.insert(code);
		data[code] += OptionData{volume, amount, strike, trades, 1};
	}

	auto vstocks = sorted(stocks, [&](const String &a, const String &b) {
			auto cmp = a.compare(0, 5, b, 0, 5);
			return cmp < 0 || (cmp == 0 && data[a].strike < data[b].strike);
	});

	cout << "option    :  strike    volume days trades\n";
	cout << "=========================================\n";

	for (auto &s : vstocks) {
		auto d = data[s];
		if (d.volume >= 7500000 && d.days > 4 && d.trades > 10) {
			cout << left << setw(10) << s << ": " << right << setw(3)
				<< Currency{d.strike} << setw(7) << Currency{d.volume}
		       		<< setw(4) << d.days  <<  setw(8) << d.trades << '\n';
		}
	}

	return 0;
}

int main(int argc, char *argv[])
{
	ifstream f;
	istream *is;

	ios::sync_with_stdio(false);
	stox_configure_sane_stdio();

	if (argc < 2)
		is = &cin;
	else {
		is = &f;
		f.open(argv[1], ios_base::in | ios_base::binary);
	}


	return run(*is);
}
