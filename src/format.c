/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include <stox/fin_parser.h>
#include <stox/fin_parser_stdio.h>
#include <stox/utils.h>

int main(int argc, char *argv[])
{
	struct stox_bovespa_format item;
	struct stox_pascal_string code;
	unsigned long long open, low, high, close;
	unsigned trades;
	unsigned long long amount, volume;
	size_t len;

	stox_configure_sane_stdio();

	if (argc < 2)
		return 1;

	len = strlen(argv[1]);

	while (fread(&item, sizeof(item), 1, stdin) > 0) {
		stox_parse_pascal_string(item.quote.code,
				sizeof(item.quote.code), &code);

		if (code.len != len || memcmp(code.str, argv[1], code.len) != 0)
			continue;

		open = stox_parse_fixed_ull(item.quote.open,
				sizeof(item.quote.open));
		low = stox_parse_fixed_ull(item.quote.low,
				sizeof(item.quote.low));
		high = stox_parse_fixed_ull(item.quote.high,
				sizeof(item.quote.high));
		close = stox_parse_fixed_ull(item.quote.close,
				sizeof(item.quote.close));
		trades = stox_parse_fixed_u(item.quote.trades,
				sizeof(item.quote.trades));
		amount = stox_parse_fixed_ull(item.quote.amount,
				sizeof(item.quote.amount));
		volume = stox_parse_fixed_ull(item.quote.volume,
				sizeof(item.quote.volume));

		printf("%.8s %.*s: ", item.quote.date, (int)code.len, code.str);
		stox_print_price_range(stdout, open, high, low, close);
		putchar(' ');
		stox_print_trading_counts(stdout, amount, volume, trades);
		putchar('\n');
	}

	return 0;
}
