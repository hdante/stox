/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>

#include <stox/fin_parser.hh>
#include <stox/utils.hh>

template <typename... Args> using UnorderedSet = std::unordered_set<Args...>;
using String = std::string;
using namespace std;
using namespace stox;

int main(int argc, char *argv[])
{
	UnorderedSet<String> stocks(100000);
	ios::sync_with_stdio(false);
	stox_configure_sane_stdio();

	ifstream f;
	istream *is;

	if (argc < 2)
		is = &cin;
	else {
		is = &f;
		f.open(argv[1], ios_base::in | ios_base::binary);
	}

	for (auto &item: BovespaTimeSeries(*is)) {
		if (item.parse_record_type() == RECORD_QUOTE)
			stocks.insert(string{item.parse_code()});
	}

	auto vgood = sorted(stocks, [&](const String &a, const String &b) {
			return a<b;
	});

	for (auto &s : vgood) {
		cout << s << '\n';
	}

	return 0;
}
