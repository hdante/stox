/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include <stox/fin_parser.h>
#include <stox/utils.h>

int main(int argc, char *argv[])
{
	struct stox_bovespa_format item;
	char *code;
	size_t len;

	stox_configure_sane_stdio();

	if (argc > 1) {
		code = argv[1];
		len = strlen(code);
	}
	else {
		code = NULL;
		len = 0;
	}

	while (fread(&item, sizeof(item), 1, stdin) > 0) {
		if (strncmp(item.quote.code, code, len) == 0 &&
				(strncmp(item.quote.bdi_type, "02", 2) == 0 ||
				strncmp(item.quote.bdi_type, "78", 2) == 0 ||
				strncmp(item.quote.bdi_type, "82", 2) == 0)) {
			fwrite(&item, sizeof(item), 1, stdout);
		}
	}

	return 0;
}
