/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <stox/fin_parser.h>
#include <stox/utils.h>

int main(void)
{
	struct stox_bovespa_format item;
	char buff[20];
	unsigned long low;

	stox_configure_sane_stdio();

	while (fread(&item, sizeof(item), 1, stdin) > 0) {
		*strchr(item.quote.code, ' ') = '\0';

		memmove(buff, &item.quote.low,
				sizeof(item.quote.low));
		buff[sizeof(item.quote.low)] = '\0';
		low = strtoul(buff, NULL, 10);

		if (low < 3) {
			printf("%.8s %.12s %.13s %.13s %.13s %.13s\n",
				item.quote.date, item.quote.code,
				item.quote.open, item.quote.low,
				item.quote.high, item.quote.close);
		}
	}

	return 0;
}
