/*
 * This file is part os the stox finance toolkit.
 * Copyright © 2018 Henrique Dante de Almeida
 *
 * stox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <optional>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <string.h>

#include <curl/curl.h>

#include <stox/fin_parser.hh>
#include <stox/fin_parser_stdio.hh>
#include <stox/utils.hh>
#include <stox/zip_file.hh>

using namespace std;
using namespace stox;

static const string BOVESPA_URL = "http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A";

typedef set<unique_ptr<BovespaFormat>, CompareByPointer<BovespaFormat>> BovespaSet;

struct Configuration {
	ifstream origin;
	bool have_file;
	int year;
	UniqueFILE current;
};

static optional<int> try_year(const char *text)
{
	try {
		auto year = stoul(text);
		return year;
	}
	catch (invalid_argument&) {
		// pass
	}

	return nullopt;
}

static optional<int> try_file(const char *text, ifstream &f)
{
	f.open(text, ios_base::in | ios_base::binary);
	regex r {R"(\d{4})"};
	cmatch m;

	auto item = BovespaTimeSeries(f).begin();
	try {
		auto type = item->parse_record_type();
		if (type == RECORD_HEADER) {
			auto file_name = item->parse_file_name();
			if(regex_search(begin(file_name), end(file_name), m, r)) {
				auto year = stoi(m[0]);
				return year;
			}
		}
	}
	catch (invalid_argument&) {
		// pass
	}

	return nullopt;
}

static string build_url(int year)
{
	return BOVESPA_URL + to_string(year) + ".ZIP";
}

static Configuration parse_configuration(int argc, char *argv[])
{
	Configuration cfg;

	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " <year|curr_file> [<new_file>]\n";
		throw invalid_argument("");
	}

	auto year = try_year(argv[1]);

	if (year) {
		cfg.have_file = false;
	}
	else {
		cfg.have_file = true;
		year = try_file(argv[1], cfg.origin);
	}

	if (!year) {
		cerr << "Invalid parameter: " << argv[1] << '\n';
		throw invalid_argument("");
	}

	cfg.year = *year;

	if (argc >= 3) {
		auto f = fopen(argv[2], "rb");
		if (f == nullptr) {
			cerr << "Unable to open file \"" << argv[2] << "\": "
				<< strerror(errno) << '\n';
			throw invalid_argument("");
		}
		cfg.current = make_unique_file(f);
	}

	return cfg;
}

static UniqueFILE download(int year)
{
	auto tmp = make_unique_file(tmpfile());
	if (tmp.get() == nullptr)
		throw runtime_error("error opening temporary file");

	if (curl_global_init(CURL_GLOBAL_ALL) != 0)
		throw runtime_error("error initializing libcurl");

	auto curl = curl_easy_init();

	curl_easy_setopt(curl, CURLOPT_URL, build_url(year).c_str());
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, tmp.get());

	cerr << "Downloading...\n";
	auto success = curl_easy_perform(curl);
	cerr << "Done.\n";

	curl_easy_cleanup(curl);
	curl_global_cleanup();

	if (success != CURLE_OK)
		throw runtime_error("error transfering file");

	return tmp;
}

static void merge(ifstream &origin, UniqueFILE &current)
{
	for (auto extracted: ZipFile(current)) {
		BovespaTimeSeries ts{origin};
		BovespaTimeSeriesZip tsz{extracted};
		auto iter1 = begin(ts);
		auto iter2 = begin(tsz);

		if (iter2 == end(tsz) || iter2->parse_record_type() != RECORD_HEADER)
			throw invalid_argument("invalid file");

		auto header_footer = *iter2++;
		cout << header_footer;

		BovespaSet elems;
		time_t curr_date = 0;
		unsigned count = 1;
		auto quote1 = QuoteIterator(iter1);
		auto quote2 = QuoteIterator(iter2);
		auto end1 = QuoteIterator<BovespaTimeSeries::Iterator>();
		auto end2 = QuoteIterator<BovespaTimeSeriesZip::Iterator>();

		while(true) {
			while(quote1 != end1 && quote1->parse_date() == curr_date)
				elems.insert(make_unique<BovespaFormat>(*quote1++));
			while(quote2 != end2 && quote2->parse_date() == curr_date)
				elems.insert(make_unique<BovespaFormat>(*quote2++));

			for (auto &e: elems) {
				cout << *e;
				count++;
			}

			elems.clear();

			if (quote1 == end1 || quote2 == end2) break;

			curr_date = min(quote1->parse_date(), quote2->parse_date());
		};

		for (; quote1 != end1; ++quote1) {
			cout << *quote1;
			count++;
		}

		for (; quote2 != end2; ++quote2) {
			cout << *quote2;
			count++;
		}

		count++;
		ostringstream scount;
		scount << setfill('0') << setw(11) << count;
		memmove(header_footer.record_type, "99", 2);
		memmove(header_footer.footer.record_count, scount.str().c_str(),
				scount.str().size());
		cout << header_footer;
	}
}

int main(int argc, char *argv[]) {
	ios::sync_with_stdio(false);
	stox_configure_sane_stdio();

	Configuration cfg;

	try {
		cfg = parse_configuration(argc, argv);
	}
	catch (invalid_argument&) {
		return 1;
	}

	cerr << "Updating year: " << cfg.year << '\n';

	try {
		if (!cfg.current) cfg.current = download(cfg.year);
		merge(cfg.origin, cfg.current);
	}
	catch (runtime_error &e) {
		cerr << "Error: " << e.what() << '\n';
		return 2;
	}

	return 0;
}
